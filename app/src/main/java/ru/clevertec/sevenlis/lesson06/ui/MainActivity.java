package ru.clevertec.sevenlis.lesson06.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.model.LatLng;

import ru.clevertec.sevenlis.lesson06.R;

public class MainActivity extends AppCompatActivity {
    public static final LatLng START_POINT = new LatLng(52.425163D, 31.015039D);
    public static final int MARKER_COUNT = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getSupportActionBar() != null)
            getSupportActionBar().hide();

        getSupportFragmentManager()
                .beginTransaction()
                .setReorderingAllowed(true)
                .disallowAddToBackStack()
                .replace(R.id.fragment_container_view, MapFragment.class, null, MapFragment.class.getSimpleName())
                .commit();
    }
}