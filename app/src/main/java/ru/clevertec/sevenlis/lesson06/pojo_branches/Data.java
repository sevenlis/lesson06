package ru.clevertec.sevenlis.lesson06.pojo_branches;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("Branch")
	private List<BranchItem> branches;

	public List<BranchItem> getBranches(){
		return branches;
	}
}