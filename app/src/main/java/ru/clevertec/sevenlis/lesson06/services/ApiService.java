package ru.clevertec.sevenlis.lesson06.services;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.GET;
import ru.clevertec.sevenlis.lesson06.pojo_atms.AtmsRoot;
import ru.clevertec.sevenlis.lesson06.pojo_branches.BranchesRoot;

public interface ApiService {
    @GET("atms")
    Observable<AtmsRoot> loadAtms();

    @GET("branches")
    Observable<BranchesRoot> loadBranches();
}
