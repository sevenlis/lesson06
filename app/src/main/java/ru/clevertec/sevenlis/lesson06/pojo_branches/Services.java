package ru.clevertec.sevenlis.lesson06.pojo_branches;

import com.google.gson.annotations.SerializedName;

public class Services{

	@SerializedName("Service")
	private Service service;

	public Service getService(){
		return service;
	}
}