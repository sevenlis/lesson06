package ru.clevertec.sevenlis.lesson06.components;

import dagger.Component;
import ru.clevertec.sevenlis.lesson06.services.ApiService;
import ru.clevertec.sevenlis.lesson06.modules.ApiServiceModule;
import ru.clevertec.sevenlis.lesson06.annotations.ApiServiceScope;

@ApiServiceScope
@Component(modules = {ApiServiceModule.class})
public interface ApiServiceComponent {
    ApiService getApiService();
}
