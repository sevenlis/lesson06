
package ru.clevertec.sevenlis.lesson06.pojo_atms;

import com.google.gson.annotations.SerializedName;

public class Geolocation {

    @SerializedName("GeographicCoordinates")
    public GeographicCoordinates geographicCoordinates;

    public GeographicCoordinates getGeographicCoordinates() {
        return geographicCoordinates;
    }
}
