package ru.clevertec.sevenlis.lesson06.models;

import static ru.clevertec.sevenlis.lesson06.ui.MainActivity.START_POINT;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import ru.clevertec.sevenlis.lesson06.classes.MarkerItem;
import ru.clevertec.sevenlis.lesson06.classes.RootItem;
import ru.clevertec.sevenlis.lesson06.components.ApiServiceComponent;
import ru.clevertec.sevenlis.lesson06.components.DaggerApiServiceComponent;
import ru.clevertec.sevenlis.lesson06.services.ApiService;

public class MarkerItemsViewModel extends ViewModel {
    private MutableLiveData<List<MarkerItem>> markersLiveData;

    public LiveData<List<MarkerItem>> getMarkersLiveData() {
        if (markersLiveData == null) {
            markersLiveData = new MutableLiveData<>();
            loadMarkers();
        }
        return markersLiveData;
    }

    private void loadMarkers() {
        final List<MarkerItem> markerItems = new ArrayList<>();

        Observer<RootItem> rootsObserver = new Observer<RootItem>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                markerItems.clear();
            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull RootItem itemRoot) {
                for (MarkerItem markerItem : itemRoot.getMarkerItems()) {
                    markerItem.setDistance(START_POINT);
                    markerItems.add(markerItem);
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                markersLiveData.setValue(markerItems);
            }
        };

        ApiServiceComponent apiServiceComponent = DaggerApiServiceComponent.builder().build();
        ApiService apiService = apiServiceComponent.getApiService();

        //объединяем только 2 потока, поскольку у api v1.0 нет методов отдельно получить банкоматы и отдельно инфокиоски
        Observable.concat(apiService.loadAtms(), apiService.loadBranches())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(rootsObserver);
    }
}
