
package ru.clevertec.sevenlis.lesson06.pojo_atms;


import com.google.gson.annotations.SerializedName;

public class ContactDetails {

    @SerializedName("phoneNumber")
    public String phoneNumber;

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
