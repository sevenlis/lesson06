package ru.clevertec.sevenlis.lesson06.pojo_branches;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Service{

	@SerializedName("88")
	private JsonMember jsonMember88;

	@SerializedName("89")
	private JsonMember jsonMember89;

	@SerializedName("90")
	private JsonMember jsonMember90;

	@SerializedName("91")
	private JsonMember jsonMember91;

	@SerializedName("92")
	private JsonMember jsonMember92;

	@SerializedName("93")
	private JsonMember jsonMember93;

	@SerializedName("94")
	private JsonMember jsonMember94;

	@SerializedName("95")
	private JsonMember jsonMember95;

	@SerializedName("96")
	private JsonMember jsonMember96;

	@SerializedName("97")
	private JsonMember jsonMember97;

	@SerializedName("10")
	private JsonMember jsonMember10;

	@SerializedName("98")
	private JsonMember jsonMember98;

	@SerializedName("11")
	private JsonMember jsonMember11;

	@SerializedName("99")
	private JsonMember jsonMember99;

	@SerializedName("12")
	private JsonMember jsonMember12;

	@SerializedName("13")
	private JsonMember jsonMember13;

	@SerializedName("14")
	private JsonMember jsonMember14;

	@SerializedName("15")
	private JsonMember jsonMember15;

	@SerializedName("16")
	private JsonMember jsonMember16;

	@SerializedName("17")
	private JsonMember jsonMember17;

	@SerializedName("18")
	private JsonMember jsonMember18;

	@SerializedName("19")
	private JsonMember jsonMember19;

	@SerializedName("0")
	private JsonMember jsonMember0;

	@SerializedName("1")
	private JsonMember jsonMember1;

	@SerializedName("2")
	private JsonMember jsonMember2;

	@SerializedName("3")
	private JsonMember jsonMember3;

	@SerializedName("4")
	private JsonMember jsonMember4;

	@SerializedName("5")
	private JsonMember jsonMember5;

	@SerializedName("6")
	private JsonMember jsonMember6;

	@SerializedName("7")
	private JsonMember jsonMember7;

	@SerializedName("8")
	private JsonMember jsonMember8;

	@SerializedName("9")
	private JsonMember jsonMember9;

	@SerializedName("20")
	private JsonMember jsonMember20;

	@SerializedName("21")
	private JsonMember jsonMember21;

	@SerializedName("22")
	private JsonMember jsonMember22;

	@SerializedName("23")
	private JsonMember jsonMember23;

	@SerializedName("24")
	private JsonMember jsonMember24;

	@SerializedName("25")
	private JsonMember jsonMember25;

	@SerializedName("26")
	private JsonMember jsonMember26;

	@SerializedName("27")
	private JsonMember jsonMember27;

	@SerializedName("28")
	private JsonMember jsonMember28;

	@SerializedName("29")
	private JsonMember jsonMember29;

	@SerializedName("30")
	private JsonMember jsonMember30;

	@SerializedName("31")
	private JsonMember jsonMember31;

	@SerializedName("32")
	private JsonMember jsonMember32;

	@SerializedName("33")
	private JsonMember jsonMember33;

	@SerializedName("34")
	private JsonMember jsonMember34;

	@SerializedName("35")
	private JsonMember jsonMember35;

	@SerializedName("36")
	private JsonMember jsonMember36;

	@SerializedName("37")
	private JsonMember jsonMember37;

	@SerializedName("38")
	private JsonMember jsonMember38;

	@SerializedName("39")
	private JsonMember jsonMember39;

	@SerializedName("CurrencyExchange")
	private List<CurrencyExchangeItem> currencyExchange;

	@SerializedName("40")
	private JsonMember jsonMember40;

	@SerializedName("41")
	private JsonMember jsonMember41;

	@SerializedName("42")
	private JsonMember jsonMember42;

	@SerializedName("43")
	private JsonMember jsonMember43;

	@SerializedName("44")
	private JsonMember jsonMember44;

	@SerializedName("45")
	private JsonMember jsonMember45;

	@SerializedName("46")
	private JsonMember jsonMember46;

	@SerializedName("47")
	private JsonMember jsonMember47;

	@SerializedName("48")
	private JsonMember jsonMember48;

	@SerializedName("49")
	private JsonMember jsonMember49;

	@SerializedName("50")
	private JsonMember jsonMember50;

	@SerializedName("51")
	private JsonMember jsonMember51;

	@SerializedName("52")
	private JsonMember jsonMember52;

	@SerializedName("53")
	private JsonMember jsonMember53;

	@SerializedName("54")
	private JsonMember jsonMember54;

	@SerializedName("55")
	private JsonMember jsonMember55;

	@SerializedName("56")
	private JsonMember jsonMember56;

	@SerializedName("57")
	private JsonMember jsonMember57;

	@SerializedName("58")
	private JsonMember jsonMember58;

	@SerializedName("59")
	private JsonMember jsonMember59;

	@SerializedName("60")
	private JsonMember jsonMember60;

	@SerializedName("61")
	private JsonMember jsonMember61;

	@SerializedName("62")
	private JsonMember jsonMember62;

	@SerializedName("63")
	private JsonMember jsonMember63;

	@SerializedName("64")
	private JsonMember jsonMember64;

	@SerializedName("65")
	private JsonMember jsonMember65;

	@SerializedName("66")
	private JsonMember jsonMember66;

	@SerializedName("67")
	private JsonMember jsonMember67;

	@SerializedName("68")
	private JsonMember jsonMember68;

	@SerializedName("69")
	private JsonMember jsonMember69;

	@SerializedName("70")
	private JsonMember jsonMember70;

	@SerializedName("71")
	private JsonMember jsonMember71;

	@SerializedName("72")
	private JsonMember jsonMember72;

	@SerializedName("73")
	private JsonMember jsonMember73;

	@SerializedName("74")
	private JsonMember jsonMember74;

	@SerializedName("75")
	private JsonMember jsonMember75;

	@SerializedName("76")
	private JsonMember jsonMember76;

	@SerializedName("77")
	private JsonMember jsonMember77;

	@SerializedName("78")
	private JsonMember jsonMember78;

	@SerializedName("79")
	private JsonMember jsonMember79;

	@SerializedName("100")
	private JsonMember jsonMember100;

	@SerializedName("101")
	private JsonMember jsonMember101;

	@SerializedName("102")
	private JsonMember jsonMember102;

	@SerializedName("103")
	private JsonMember jsonMember103;

	@SerializedName("80")
	private JsonMember jsonMember80;

	@SerializedName("81")
	private JsonMember jsonMember81;

	@SerializedName("82")
	private JsonMember jsonMember82;

	@SerializedName("83")
	private JsonMember jsonMember83;

	@SerializedName("84")
	private JsonMember jsonMember84;

	@SerializedName("85")
	private JsonMember jsonMember85;

	@SerializedName("86")
	private JsonMember jsonMember86;

	@SerializedName("87")
	private JsonMember jsonMember87;


	public List<CurrencyExchangeItem> getCurrencyExchange(){
		return currencyExchange;
	}

}