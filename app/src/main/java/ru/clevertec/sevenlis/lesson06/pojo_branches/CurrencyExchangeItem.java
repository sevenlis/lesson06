package ru.clevertec.sevenlis.lesson06.pojo_branches;

import com.google.gson.annotations.SerializedName;

public class CurrencyExchangeItem{

	@SerializedName("dateTime")
	private String dateTime;

	@SerializedName("ExchangeTypeStaticType")
	private String exchangeTypeStaticType;

	@SerializedName("scaleCurrency")
	private String scaleCurrency;

	@SerializedName("targetCurrency")
	private String targetCurrency;

	@SerializedName("exchangeRate")
	private String exchangeRate;

	@SerializedName("sourceCurrency")
	private String sourceCurrency;

	@SerializedName("direction")
	private String direction;

	public String getDateTime(){
		return dateTime;
	}

	public String getExchangeTypeStaticType(){
		return exchangeTypeStaticType;
	}

	public String getScaleCurrency(){
		return scaleCurrency;
	}

	public String getTargetCurrency(){
		return targetCurrency;
	}

	public String getExchangeRate(){
		return exchangeRate;
	}

	public String getSourceCurrency(){
		return sourceCurrency;
	}

	public String getDirection(){
		return direction;
	}
}