package ru.clevertec.sevenlis.lesson06.ui;

import static ru.clevertec.sevenlis.lesson06.ui.MainActivity.START_POINT;
import static ru.clevertec.sevenlis.lesson06.ui.MainActivity.MARKER_COUNT;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ru.clevertec.sevenlis.lesson06.R;
import ru.clevertec.sevenlis.lesson06.classes.MarkerItem;
import ru.clevertec.sevenlis.lesson06.models.MarkerItemsViewModel;

public class MapFragment extends SupportMapFragment implements OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;
    private ProgressBar progressBar;

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        progressBar = activity.findViewById(R.id.progress_bar);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getMapAsync(this);
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;

        UiSettings settings = mMap.getUiSettings();
        settings.setAllGesturesEnabled(true);
        settings.setZoomControlsEnabled(true);
        settings.setZoomGesturesEnabled(true);
        settings.setMyLocationButtonEnabled(true);

        mMap.addMarker(new MarkerOptions()
                .title("Start position")
                .snippet(START_POINT.latitude + ", " + START_POINT.longitude)
                .position(START_POINT)
        );
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(START_POINT,15.0f),1000,null);

        progressBar.setVisibility(View.VISIBLE);
        MarkerItemsViewModel model = new ViewModelProvider(this).get(MarkerItemsViewModel.class);
        model.getMarkersLiveData().observe(this, new androidx.lifecycle.Observer<List<MarkerItem>>() {
            @Override
            public void onChanged(List<MarkerItem> markerItems) {
                MapFragment.this.addMarkers(markerItems);
                progressBar.setVisibility(View.GONE);
            }
        });

        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
    }

    private void addMarkers(List<MarkerItem> markerItems) {
        Comparator<MarkerItem> distanceComparator = new Comparator<MarkerItem>() {
            @Override
            public int compare(MarkerItem markerItem1, MarkerItem markerItem2) {
                return Double.compare(markerItem1.getDistance(), markerItem2.getDistance());
            }
        };
        Collections.sort(markerItems, distanceComparator);

        for (MarkerItem markerItem : markerItems.subList(0, MARKER_COUNT)) {
            MarkerOptions markerOptions = new MarkerOptions()
                    .icon(markerItem.getIconDescriptor())
                    .position(markerItem.getLatLng())
                    .anchor(0.5f, 1.0f)
                    .title(markerItem.getAddress())
                    .snippet(markerItem.getDescription() + " " + markerItem.getType());

            mMap.addMarker(markerOptions);
        }
    }

    @Override
    public boolean onMarkerClick(@NonNull Marker marker) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 20.0f),1000,null);
        marker.showInfoWindow();
        return true;
    }


    @Override
    public void onInfoWindowClick(@NonNull Marker marker) {
        Toast.makeText(getActivity(), marker.getTitle() + "\n" + marker.getSnippet(), Toast.LENGTH_LONG).show();
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 15.0f),1000,null);
        //marker.hideInfoWindow();
    }
}
