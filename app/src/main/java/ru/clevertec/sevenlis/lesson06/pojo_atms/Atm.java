
package ru.clevertec.sevenlis.lesson06.pojo_atms;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Atm {

    @SerializedName("atmId")
    public String atmId;
    @SerializedName("type")
    public String type;
    @SerializedName("baseCurrency")
    public String baseCurrency;
    @SerializedName("currency")
    public String currency;
    @SerializedName("cards")
    public List<String> cards = null;
    @SerializedName("currentStatus")
    public String currentStatus;
    @SerializedName("Address")
    public Address address;
    @SerializedName("Services")
    public List<Service> services = null;
    @SerializedName("Availability")
    public Availability availability;
    @SerializedName("ContactDetails")
    public ContactDetails contactDetails;

    public String getAtmId() {
        return atmId;
    }

    public String getType() {
        return type;
    }

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public String getCurrency() {
        return currency;
    }

    public List<String> getCards() {
        return cards;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public Address getAddress() {
        return address;
    }

    public List<Service> getServices() {
        return services;
    }

    public Availability getAvailability() {
        return availability;
    }

    public ContactDetails getContactDetails() {
        return contactDetails;
    }
}
