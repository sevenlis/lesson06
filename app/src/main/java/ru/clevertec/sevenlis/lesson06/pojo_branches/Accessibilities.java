package ru.clevertec.sevenlis.lesson06.pojo_branches;

import com.google.gson.annotations.SerializedName;

public class Accessibilities{

	@SerializedName("Accessibility")
	private Accessibility accessibility;

	public Accessibility getAccessibility(){
		return accessibility;
	}
}