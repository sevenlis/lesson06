package ru.clevertec.sevenlis.lesson06.pojo_branches;

import com.google.gson.annotations.SerializedName;

public class SocialNetworksItem{

	@SerializedName("networkName")
	private String networkName;

	@SerializedName("description")
	private String description;

	@SerializedName("url")
	private String url;

	public String getNetworkName(){
		return networkName;
	}

	public String getDescription(){
		return description;
	}

	public String getUrl(){
		return url;
	}
}