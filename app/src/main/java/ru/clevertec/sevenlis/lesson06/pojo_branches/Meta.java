package ru.clevertec.sevenlis.lesson06.pojo_branches;

import com.google.gson.annotations.SerializedName;

public class Meta{

	@SerializedName("TotalPages")
	private String totalPages;

	public String getTotalPages(){
		return totalPages;
	}
}