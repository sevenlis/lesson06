package ru.clevertec.sevenlis.lesson06.classes;

import java.util.List;

import ru.clevertec.sevenlis.lesson06.classes.MarkerItem;

public interface RootItem {
    List<MarkerItem> getMarkerItems();
}
