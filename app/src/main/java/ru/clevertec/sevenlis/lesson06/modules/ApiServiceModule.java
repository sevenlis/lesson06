package ru.clevertec.sevenlis.lesson06.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.clevertec.sevenlis.lesson06.services.ApiService;
import ru.clevertec.sevenlis.lesson06.annotations.ApiServiceScope;

@Module
public class ApiServiceModule {
    private static final String BASE_URL = "https://belarusbank.by/open-banking/v1.0/";

    @Provides
    public ApiService apiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @ApiServiceScope
    @Provides
    public Retrofit retrofit(GsonConverterFactory gsonConverterFactory, RxJava3CallAdapterFactory rxJava3CallAdapterFactory) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJava3CallAdapterFactory)
                .build();
    }

    @Provides
    public GsonConverterFactory gsonConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    public Gson gson() {
        return new GsonBuilder()
                .setLenient()
                .create();
    }

    @Provides
    public RxJava3CallAdapterFactory rxJava3CallAdapterFactory() {
        return RxJava3CallAdapterFactory.create();
    }
}
