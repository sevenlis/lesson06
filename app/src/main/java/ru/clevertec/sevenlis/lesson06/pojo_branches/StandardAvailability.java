package ru.clevertec.sevenlis.lesson06.pojo_branches;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class StandardAvailability{

	@SerializedName("Day")
	private List<DayItem> days;

	public List<DayItem> getDays(){
		return days;
	}
}