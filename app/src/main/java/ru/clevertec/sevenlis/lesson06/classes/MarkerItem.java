package ru.clevertec.sevenlis.lesson06.classes;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import ru.clevertec.sevenlis.lesson06.R;
import ru.clevertec.sevenlis.lesson06.pojo_atms.Atm;
import ru.clevertec.sevenlis.lesson06.pojo_branches.BranchItem;

public class MarkerItem {
    private final BitmapDescriptor iconDescriptor;
    private final LatLng latLng;
    private final String type;
    private final String description;
    private final String address;
    private double distance, bearing;

    public MarkerItem(Atm atm) {
        iconDescriptor = BitmapDescriptorFactory.fromResource(R.mipmap.ic_bsb_atm);
        latLng = new LatLng(
                Double.parseDouble(atm.getAddress().getGeolocation().getGeographicCoordinates().getLatitude()),
                Double.parseDouble(atm.getAddress().getGeolocation().getGeographicCoordinates().getLongitude())
        );
        type = atm.getType();
        description = atm.getAddress().getAddressLine();
        address = atm.getAddress().getAddressName();
    }

    public MarkerItem(BranchItem branch) {
        iconDescriptor = BitmapDescriptorFactory.fromResource(R.mipmap.ic_bsb_branch);
        latLng = new LatLng(
                Double.parseDouble(branch.getAddress().getGeoLocation().getGeographicCoordinates().getLatitude()),
                Double.parseDouble(branch.getAddress().getGeoLocation().getGeographicCoordinates().getLongitude())
        );
        type = branch.getName();
        description = branch.getAddress().getDescription();
        address = branch.getAddress().getAddressName();
    }

    public void setDistance(LatLng toLatLng) {
        double startLong = getLatLng().longitude;
        double startLat = getLatLng().latitude;
        double endLong = toLatLng.longitude;
        double endLat = toLatLng.latitude;

        // Константы, используемые для вычисления смещения и расстояния
        double D2R  = 0.017453; // Константа для преобразования градусов в радианы
        double R2D  = 57.295781; // Константа для преобразования радиан в градусы
        double a 	= 6378137.0; // Основные полуоси
        //double b 	= 6356752.314245; // Неосновные полуоси
        double e2 	= 0.006739496742337; // Квадрат эксцентричности эллипсоида
        //double f 	= 0.003352810664747; // Выравнивание эллипсоида

        double fdLambda = (startLong - endLong) * D2R;
        double fdPhi = (startLat - endLat) * D2R;
        double fPhimean = ((startLat + endLat) / 2.0) * D2R;

        // Вычисляем меридианные и поперечные радиусы кривизны средней широты
        double fTemp = 1 - e2 * (Math.pow(Math.sin(fPhimean), 2));
        double fRho = (a * (1 - e2)) / Math.pow(fTemp, 1.5);
        double fNu = a / (Math.sqrt(1 - e2 * (Math.sin(fPhimean) * Math.sin(fPhimean))));

        // Вычисляем угловое расстояние
        double fz = Math.sqrt(Math.pow(Math.sin(fdPhi / 2.0), 2) + Math.cos(endLat * D2R) * Math.cos(startLat * D2R) * Math.pow(Math.sin(fdLambda / 2.0), 2));
        fz = 2 * Math.asin(fz);

        // Вычисляем смещение
        double fAlpha = Math.cos(endLat * D2R) * Math.sin(fdLambda) * 1 / Math.sin(fz);
        fAlpha = Math.asin(fAlpha);

        // Вычисляем радиус Земли
        double fR = (fRho * fNu) / ((fRho * Math.pow(Math.sin(fAlpha), 2)) + (fNu * Math.pow(Math.cos(fAlpha), 2)));

        // Получаем расстояние и смещение
        distance = (fz * fR); // метры

        double absAD = Math.abs(fAlpha * R2D);

        if ((startLat < endLat) && (startLong < endLong)) {
            bearing =  absAD;
        } else if ((startLat < endLat) && (startLong > endLong)) {
            bearing = 360 - absAD;
        } else if ((startLat > endLat) && (startLong > endLong)) {
            bearing = 180 + absAD;
        } else if ((startLat > endLat) && (startLong < endLong)) {
            bearing = 180 - absAD;
        }
    }

    public BitmapDescriptor getIconDescriptor() {
        return iconDescriptor;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public String getAddress() {
        return address;
    }

    public double getDistance() {
        return distance;
    }

    public double getBearing() {
        return bearing;
    }
}
